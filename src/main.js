var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 29
var array = create2DArray(arraySize, arraySize, 0, false)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * boardSize * 0.025, windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * boardSize * 0.025)
      noStroke()
      fill(255 - 128 * (2 * sin(frameCount * 0.1 + 2.6 * (i + 1.2) * (j + 2.3) * array[i][j])))
      ellipse(0, 0, boardSize * 0.01 * (2 * sin(frameCount * 0.1 + (2.193 + (mouseX / windowWidth + mouseY / windowHeight) * 0.01) * (i + 2.712) * (j + 2.385) * array[i][j])))
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.random()
      }
    }
    array[i] = columns
  }
  return array
}
